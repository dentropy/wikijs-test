---
share: true
uuid: 146656b4-573a-4e42-8f00-239ab29eac3b
title: index
---
Dentropy's Personal Wiki,

I like to work on projects, consume media that makes you think, and attempting to deconstruct big ideas.


* [Blog Posts and Videos](/0709dea0-4a97-4596-8d8e-32a0e614f8a2)
* [My Projects](/e76c8ac9-69f3-477f-8015-556e83738432)
* [Tutorials and Learning Pathways](/5ba9b0de-6aad-48fa-ab60-ae39c62a027a)
* [Favorite Software](/6a24cf3e-5693-4b99-b620-c3766a02a6c9)
* [Favorite Media](/cf6a4db5-dcac-48ae-97ec-cf40f28e2b20)