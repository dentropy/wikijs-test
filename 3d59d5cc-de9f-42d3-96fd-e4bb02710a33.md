---
share: true
uuid: 3d59d5cc-de9f-42d3-96fd-e4bb02710a33
title: Blog Posts
---
## Writings

* [Virtualization The Self](/3fdd115e-53ba-40fe-af63-e5e19b97f763)
	* Imagine your mind as a hypervisor that can run your personality inside a virtual machine.
* [Learning to sail the memes](/e3ed979d-7207-4dfa-806c-03aab973a4c9)
	* Feeling where you are so you know where you want to be
* [Cringe meets theory of mind](/cef3066c-8a14-467b-979e-9036068e3653)
	* Forcing yourself to feel
* [You took the Transhumanist Wager Now What?](/34608d94-e304-4aa4-9339-f23d3fa39359)
	* An attempt at outrunning death is going to require a lot more than just desire
* [Consciousness and Parasites](/b31360a6-ae1d-45d2-95e1-cd884a27971f)
	* If the self can be broken down into component parts what constitutes one's identity?
* [The Daemon is Real, Now What?](/952abb04-36df-4913-ace1-651763fa1c88)
	* You may already be living in a digital [panopticon](/undefined), what are the prison guards looking for in terms of behavior?
* [How Does One Go About Wielding Their Own Plot Armor?](/7de2eb48-087b-4936-8446-dcc021b74966)
	* Your life is a narrative, care to take control of it?

## Collections

* [Inital Writings](/c4747e80-98b8-4dca-93d9-14d4e6425e70)
* [Mapping out Self Actualization](/6d0bbf21-e1ea-4a09-9597-ec479b998235)
* [Videos and Their Scripts](/b6611f4f-b019-4676-902e-8ea82840d740)