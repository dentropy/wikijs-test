---
share: true
uuid: b554fe38-0be3-4e5e-a817-41077f5f6e69
title: Dentropy's Tutorials
---
Before reading through the tutorials I have already documented you may want to check out [Learning Pathways](/10708552-def9-4391-9126-8a4f53cb5e00) where I map out how different skills and projects connect to one another.

## List of Tutorials

* [S3 Tutorial](/undefined)
* [SQL](/9bf437f1-b997-4df7-9cb5-d1dcb65fb892)
	* [Why you should learn SQL](/d0531e52-f4bb-4dd1-ac6f-6d188a4d3be6)
	* [Recommended SQL Tutorials](/812dc6f0-a6db-4fd6-9bd2-707d8a0483f5)
	* [Online SQL Consoles](/72122562-a2c3-4a1f-913b-ce02ab0c276b)
	* [Dentropys's SQL Alchemy Tutorial](/34aa710f-0d0e-4098-88aa-e0b554a2298e)
	* [JSON in sqlite](/b1112011-a44d-4764-bff7-21b74dc2e57c)
	* [Ideas for SQL Projects](/bc09af66-30fa-481e-ba9b-b5cb7ac469d8)
* [Python](/80428ac9-197a-4c70-9230-119cf9079782)
	* [Why you should learn Python](/74ed05f7-5fb9-4faa-b6b9-fd8dfe24907e)
	* [Recommended Python Tutorials](/50fb7356-1b5d-4c8a-81c9-a0adcd9c5b2d)
	* [Online Python Interpreters](/fd372b7b-3dd1-4760-b25c-87c84dd91c5a)
* [HTML](/272babbb-b019-4290-941a-01ae25d07fe1)
	* [Why you should learn HTML](/7e8c26c2-22d4-4d7c-9bdf-5f38b7815f85)
	* [Recommended HTML Tutorials](/8b4e3a9a-b83c-49c9-96f6-c39682365471)
	* [Online HTML Editors](/1b1b313e-4530-432c-9aff-bbfce704e774)
* [Javascript](/e4f5fb54-c63f-4567-851b-e61a4a58037d)
	* [Why you should learn JavaScript](/55c52cc9-ff22-4a73-afbc-4a2b953537a7)
	* [Recommended JavaScript Tutorials](/e333e392-4ae0-4419-b401-6d09929f38f9)
	* [Online JavaScript Editor](/6d792e32-10bb-46f5-b214-fe7dedacfbb4)
* [Homelab and SysAdmin Skills](/29d7fc31-bf16-4efb-90b2-58dae5c546e3)
	* [How to mount a partition in Linux?](/undefined)
	* [Create a Multi ISO USB Drive and document the process as a tutorial](/69a15b0e-608e-4f59-ba71-a4b159ca12a0)
* Project Tutorials
	* [Web Scraping Tutorial](/83ffe54f-3356-44ae-9d1a-878ef448fb57)
* [Umbrel](/60722662-eccc-443d-af35-af0ee02d1c9c)
	* [Umbrel - Secure Install](/c14c9c80-6039-4bf8-bb72-0afbaceb08ea)
	* [Umbrel - Backup and Restore](/92aa8e61-712a-414d-95c1-7b9ff98c2f98)
	* [Umbrel - Migrate App](/06913657-30a0-4e59-98b1-42371710dafb)
* TODO
	* Database Backup and Restore
	* Docker Networking
	* CLI Torrent Client with VPN
	* S3 / Minio Tutorial
	* Pub Sub
		* Rabbit MQ, Nats, Kafka
	* Data Streaming
		* Kafka, Nats Jetstream
	* Data Lake
		* Presto
		* [How Discord Stores Trillions of Messages | Deep Dive - YouTube](https://www.youtube.com/watch?v=xynXjChKkJc)
	* Changing networking in live environment
	* Natural Language Search
		* [Meilisearch](/91735b8b-9efc-4e78-97ab-254ee418a01e)
	* [NLP](/5cd22bfe-14f1-4724-9560-95a24b8cb849)
		* [Natural Language Processing Demystified](https://www.nlpdemystified.org/)
	* Deploying [Funky Penguin](/undefined)'s Software
	* [Dentropy's Ideal DevSecOps Stack](/406a13ea-5f64-440a-b454-6b43afe9e0d5)