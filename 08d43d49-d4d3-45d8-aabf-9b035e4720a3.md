---
uuid: 08d43d49-d4d3-45d8-aabf-9b035e4720a3
share: true
title: Obsidian Plugin find-unlinked-files
---
* [Vinzent03/find-unlinked-files: Find files, which are nowhere linked, so they are maybe lost in your vault.](https://github.com/Vinzent03/find-unlinked-files)