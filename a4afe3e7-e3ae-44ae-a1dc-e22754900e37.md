---
uuid: a4afe3e7-e3ae-44ae-a1dc-e22754900e37
share: true
title: ETL to QE, Update 3, Progress on first Report to be Published
---
Date: [2023-10-04](/undefined)

See [Discord Binding](/16cc922f-56ea-422e-95be-72f5f55e4111) for project context
## Progress on first Report to be Published

I want to be able to generate both a generalized report summarizing all the Discord Guilds I have scraped as well as guild specific reports. Today I created a list of the Queries and Data Visualizations I want to in the generalized report in which can be found in [Questions to Answer in General Report to Compare Discord Guilds](/0c4bbdac-febf-4e8e-861f-c36ef88a71c9).

I need data sorted and in a specific format before I can fit it into [plotly](/undefined) the [Data Visualization](/ef29cab3-4aef-413f-b603-29cfeedd290d) software I am currently using inside a [jupyter](/14b19809-58b0-44c8-a719-c50badebb08c) notebook. I have been tempted to resolve SQL queries then use python to synthesize the data into the order and format I need for [plotly](/undefined) but instead I decided to do as much in SQL as I can. I decided to do as much in SQL as possible because it will later allow me to possibly choose a different programming language from python for my backend, such as JavaScript, Go, Java, or Rust.

I also made an interesting discovery today using [Plotly to produce multiple lines on a line chart](https://plotly.com/python/line-charts/). Turns out you can simply have one a dataframe with one column for labels, one for the timestamp, and one for the whatever value I want. I just point [plotly](/undefined) at the right column on the dataframe and I don't need to think about parsing the data in any way myself. For reference check out, [How to use Pandas and Plotly together?](/undefined)