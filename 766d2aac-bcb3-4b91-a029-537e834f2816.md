---
uuid: 766d2aac-bcb3-4b91-a029-537e834f2816
share: true
title: Questions for DAO's
---
* [Who are you trying to coordinate?](/undefined)
* [What are you trying to produce?](/undefined)
	* [What are the steps to produce that?](/undefined)
	* [How is this currently being produced?](/undefined)
* [ Who will find value in what the DAO produces?](/undefined)
* [Who will be the first people to produce for the DAO?](/undefined)

## Links

* [DAO](/26725b10-b472-41ee-ba3b-4e9bc851f174)