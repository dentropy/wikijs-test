---
share: true
uuid: eba2dd18-4cda-4260-bdde-c4b14038af1c
title: twitter-scraper
---
## WORKS vladkens/twscrape

* Code: [vladkens/twscrape: 2023! Twitter API scrapper with authorization support. Allows you to scrape search results, User's profiles (followers/following), Tweets (favoriters/retweeters) and more.](https://github.com/vladkens/twscrape)
* Results:
	* CLI does not work for adding accounts, but the default Python method with the cookies works like a charm
## FAIL mahrtayyab/tweety

* Code: [mahrtayyab/tweety: Twitter Scraper](https://github.com/mahrtayyab/tweety)
* Docs: [Tweety’s Documentation — Tweety 0.9.5 documentation](https://mahrtayyab.github.io/tweety_docs/)
* Results:
	* Can't import the dam thing

## FAIL n0madic/twitter-scraper
* Code: [n0madic/twitter-scraper: Scrape the Twitter Frontend API without authentication with Golang.](https://github.com/n0madic/twitter-scraper)

``` bash

go install github.com/n0madic/twitter-scraper@latest

```