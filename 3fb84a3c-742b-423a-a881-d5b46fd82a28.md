---
uuid: 3fb84a3c-742b-423a-a881-d5b46fd82a28
share: true
title: What percentage of days actually have messages?
---
See [How to get message count by day for a single discord guild?](/fe3d485e-3f96-4cbd-8a63-2feda6021e8e)

## All Guilds SQL Query

#TODO Practice [SQL](/9bf437f1-b997-4df7-9cb5-d1dcb65fb892) challenge

## Single Guild SQL Query


``` SQL

select 
	id,
	guild_name,
	min(day_timestamp)::DATE as earliest_date,
	max(day_timestamp)::DATE as latest_date,
	count(*) as days_with_messages,
    max(day_timestamp)::DATE - min(day_timestamp)::DATE as total_num_of_days,
    cast( count(*) as FLOAT) / cast( ( max(day_timestamp)::DATE - min(day_timestamp)::DATE ) as Float)* 100 as percentage_of_days
from
(
	select 
		distinct 
			guilds_t.id,
			guilds_t.guild_name,
			day_timestamp,
			msg_count 
	from (
		select distinct
			DATE_TRUNC('day', real_timestamp) AS  day_timestamp,
		    COUNT(guild_id) AS msg_count,
		    guild_id 
		FROM messages_t
		WHERE messages_t.guild_id = (SELECT id from guilds_t LIMIT 1 offset 0)
		GROUP BY guild_id, day_timestamp
	) as month_messages_t
	join guilds_t on month_messages_t.guild_id = guilds_t.id
	order by day_timestamp desc
) as daily_msg_stats_t
group by id, guild_name;

```