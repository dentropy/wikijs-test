---
uuid: a6649a28-5862-4820-8797-1cfaf2ba1713
share: true
title: Tasks for ETL to Question Engine
---
## Phase 1

* Infrastructure
	* [Get all raw data in one location - ETLtoQE](/undefined)
	* [Get a dedicated Postgres server - ETLtoQE](/undefined)
* ETL
	* [Decide on ETL procedure and document it - ETLtoQE](/undefined)
	* [Create a single script for managing all ETL steps - ETLtoQE](/undefined)
* Raw Analytics
	* [Publish the first generalised report using a Jupyter notebook - ETLtoQE](/undefined)
	* [Generate Guild Specific Reports - ETLtoQE](/undefined)
* Front end
	* [Get the Dashboard up and running with Basic Auth - ETLtoQE](/undefined)

## Phase 2

* [Add JSON Web Token Authentication to Dashboard App - ETLtoQE](/undefined)
* Data Contextualization
	* [Decide on a Graph Database and write a Blog Post about it - ETLtoQE](/undefined)
	* Design and decide on Graph schema for Discord Data Annotation
	* Design API endpoints for annotating Graph Data

In [Update 12](/20be1355-e585-4eb4-b0a7-4a65c1eda264) I got feedback recommending I look for key words within the Discord Dataset. Now I need to come up with a narrow question or problem that involves key word queries.