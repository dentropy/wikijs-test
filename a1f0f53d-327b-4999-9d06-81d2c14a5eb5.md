---
uuid: a1f0f53d-327b-4999-9d06-81d2c14a5eb5
share: true
title: What discord guild's have the most Users?
---
## Data Visualization

* Bar Graph

## SQL Query

``` SQL

select 
	guilds_t.id, 
	guilds_t.guild_name, 
	guild_author_count_t.author_count 
FROM 
(
	select distinct guild_id, COUNT(distinct(author)) as author_count
	from messages_t mt 
	group by guild_id
) as guild_author_count_t
join guilds_t on guild_author_count_t.guild_id = guilds_t.id
order by guild_author_count_t.author_count asc;

```