---
uuid: cf6a4db5-dcac-48ae-97ec-cf40f28e2b20
share: true
title: Favorite Media
---
* [Media Consumption Backlog](/78aa36ca-c4c6-40ed-873c-24099d5c2481)
## [Non Fiction Books](/undefined)

* [Homo Deus](/2055ffd4-310b-4f0f-b7f8-61d91402650c)
* [The Singularity Is Near](/051e7911-5b2a-43e8-89be-899447d13e80)
* [Human Forever](/undefined)
* [12 Rules For Life](/550c359a-b6b9-45f7-87f0-3325be73cf59)

## [Fiction Books](/undefined)

* [Altered Carbon](/b541230e-3354-44a6-ab43-917335802cf5)
* [The Culture Series](/45ae90e1-c4fd-4d7a-b290-a4050b37b573)
* [Hyperion](/5e1088f4-7428-487f-9300-86f478f05dca)
* [Daemon by Daniel Suarez](/96e5b489-1dc8-47ed-a058-25ac9da1cd40)
* [Dungeon Crawler Carl](/undefined)
## [Movies](/67e55d56-5eac-48d2-890f-04fc0a970d02)

* [Blade Runner](/undefined)
* [Fight Club](/undefined)

## [TV Shows](/1f9b39e7-663b-46f3-951f-f66a9cd360e5)

* [Person of Interest](/91c882b1-420a-4a1d-bab9-ae264ea6071f)
* [Westworld](/03515a8a-a40a-48fd-9304-1565acfd1ce2)

## [Video Games](/d5de46c0-134d-4329-b3b5-5783f6c2c2e9)

* [Factorio](/undefined)
* [Satisfactory](/b506fd7f-c2b2-4e4c-b0e7-873ba6dc6a1c)
* [Starcraft 2](/undefined)
* [Grand Theft Auto 5](/undefined)

## [Anime](/a0b15bdd-022a-4893-b12a-db25bfb5e041)

* [Ghost in The Shell](/undefined)
* [Neon Genesis Evangelion](/a6a1addb-06e1-48c3-a100-2ace7697354e)
* [Madoka Magica](/14b11fef-070f-4850-a102-3d4d3a7b107b)
* [Kino's Journey](/undefined)
* [Psycho Pass](/undefined)
* [Violet Evergarden](/undefined)
* [Re Creators](/7871fe55-e7f2-4255-9ffc-95b89f393656)
* [Ergo Proxy](/undefined)