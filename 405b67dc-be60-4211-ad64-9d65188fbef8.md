---
uuid: 405b67dc-be60-4211-ad64-9d65188fbef8
share: true
title: ArchiveBox
---
[ArchiveBox | 🗃 Open source self-hosted web archiving. Takes URLs/browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more…](https://archivebox.io/)