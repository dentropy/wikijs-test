---
uuid: ae8cb99d-65d5-404f-9d83-4572cca17719
share: true
title: What percentage of all messages in Discord Guild came from 1, 10, and 100
  Authors?
---
## See Related Queries

- [What authors posted the most in each discord guild?](/34592fa9-bd8d-4237-bdff-36cb58fdc21e)
 - [What is the average half life of top 30% active users?](/4f6a01a0-6799-43a6-b36a-38edd59d36fc)