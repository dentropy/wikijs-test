---
uuid: 0c4bbdac-febf-4e8e-861f-c36ef88a71c9
share: true
title: Questions to Answer in General Report to Compare Discord Guilds
---
1. [✅] [Order discord Guild's by the oldest message?](/f87ebf9c-0d6e-4224-9418-badaa496ce2f)
2. [✅] [What discord guilds have the most messages?](/2e369507-1f65-4c97-9a37-375f2b5d27fb)
3. [✅] [What discord guild's have the most Users?](/a1f0f53d-327b-4999-9d06-81d2c14a5eb5)
4. [✅] [What discord guild's have the most channels?](/undefined)
5. [✅] [What percentage of users on each Discord Guild posted less than 1, 3, 10, or 100 messages?](/fb38895a-faca-42e7-8e40-416fe115a9ee)
6. [🌗] [What percentage of all messages in Discord Guild came from 1, 10, and 100 Authors?](/ae8cb99d-65d5-404f-9d83-4572cca17719)
	1. This query requires bot labelling to be effective, query done, not included in report
	2. [What authors posted the most in each discord guild?](/34592fa9-bd8d-4237-bdff-36cb58fdc21e)
8. [✅] [What is the activity per month of each discord guild measured in messages per month?](/edb39918-b02f-4ee7-b2b2-d902c8370412)
9. [✅] [What is the activity of each month for each guild normalised on a single graph?](/8b9a34f5-a141-47be-ab51-091a0e05339b)
10. [✅] [What is the name of the most active channel in each discord guild?](/27802970-c6dc-462e-8210-216bb1eb6a36)
	1. [What is the most active channel in a discord guild?](/45f50e6a-fb81-4f7c-87b6-70785da72633)
	2. [When was the most active month for the most active channel in each discord guild?](/30d09691-d6bc-462d-b9ae-8534e88e4cf1)
11. [✅] [What is the average message count for most active X days of a Discord Guild?](/9a9414ed-c3bd-433e-bb5b-4732aff405a0)
	1. [How to get message count by day for a single discord guild?](/fe3d485e-3f96-4cbd-8a63-2feda6021e8e)
12. [✅] [What percentage of days actually have messages?](/3fb84a3c-742b-423a-a881-d5b46fd82a28)
13. [✅] [What is the average half life of top 30% active users?](/4f6a01a0-6799-43a6-b36a-38edd59d36fc)